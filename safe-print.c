#include <stdarg.h>
#include <stdio.h>

#include "safe-print.h"

void safe_print(char *fmt, ...) {
	va_list va;
	va_start(va, fmt);
	WaitForSingleObject(print_mutex, INFINITE);
	vprintf(fmt, va);
	ReleaseMutex(print_mutex);
	va_end(va);
}
