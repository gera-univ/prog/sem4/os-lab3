#include <stdio.h>

#include "marker.h"

#include "events.h"
#include "safe-print.h"

DWORD WINAPI do_work(LPVOID n) {
	int* id = n;
	srand(*id);

	int marked_count = 0;
	
	WaitForSingleObject(events_start[*id - 1], INFINITE);

	safe_print("%d started\n", *id);

	HANDLE possible_events[2] = {events_terminate[*id - 1], events_start[*id - 1]};

	SetEvent(events_ready[*id - 1]);
	
	while (TRUE) {
		int index = rand() % array_size;

		WaitForSingleObject(array_mutex, INFINITE);
		BOOL condition = array[index] == 0;
		ReleaseMutex(array_mutex);
		
		if (condition) {
			Sleep(5);
			WaitForSingleObject(array_mutex, INFINITE);
			array[index] = *id;
			ReleaseMutex(array_mutex);
			++marked_count;
			Sleep(5);
		} else {
			safe_print("%d blocked on element %d, %d elements marked\n", *id, index, marked_count);
			SetEvent(events_blocked[*id - 1]);
			DWORD result = WaitForMultipleObjects(2, possible_events, FALSE, INFINITE);
			if (result == 0) {
				WaitForSingleObject(array_mutex, INFINITE);
				for (int i = 0; i < array_size; ++i) {
					if (array[i] == *id) {
						array[i] = 0;
					}
				}
				ReleaseMutex(array_mutex);
				break;
			}
			ResetEvent(events_blocked[*id - 1]);
			SetEvent(events_ready[*id - 1]);
		}
	}

	safe_print("%d terminated\n", *id);
	
	return 0;
}
