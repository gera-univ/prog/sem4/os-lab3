#include "events.h"

BOOL create_events(int thread_count) {
	events_start = malloc(sizeof(HANDLE) * thread_count);
	events_blocked = malloc(sizeof(HANDLE) * thread_count);
	events_terminate = malloc(sizeof(HANDLE) * thread_count);
	events_ready = malloc(sizeof(HANDLE) * thread_count);
	if (!events_blocked)
		return FALSE;
	for (int i = 0; i < thread_count; ++i) {
		events_start[i] = CreateEvent(NULL, FALSE, FALSE, NULL);
		events_blocked[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
		events_terminate[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
		events_ready[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
	}
	return TRUE;
}

void delete_events(int thread_count) {
	for (int i = 0; i < thread_count; ++i) {
		CloseHandle(events_start[i]);
		CloseHandle(events_blocked[i]);
		CloseHandle(events_terminate[i]);
		CloseHandle(events_ready[i]);
	}
	free(events_start);
	free(events_blocked);
	free(events_terminate);
	free(events_ready);
}
