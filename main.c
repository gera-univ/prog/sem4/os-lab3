#include <stdio.h>
#include <stdlib.h>

#include "marker.h"
#include "events.h"
#include "safe-print.h"

int thread_count = 0;

DWORD ask_terminate(BOOL* alive_threads) {
	Sleep(100);
	WaitForSingleObject(print_mutex, INFINITE);
	printf("All threads are blocked.\nWhich thread to terminate?\n");
	DWORD result;
	scanf_s("%lu", &result);
	ReleaseMutex(print_mutex);
	if (alive_threads[result - 1] == FALSE) {
		safe_print("Thread %lu is already dead\n", result);
		return ask_terminate(alive_threads);
	}
	return result - 1;
}

int main() {
	printf("enter array size: ");
	scanf_s("%d", &array_size);
	if (array_size <= 0) {
		fprintf(stderr, "Incorrect array size\n");
		return 1;
	}

	array = malloc(array_size * sizeof(int));
	if (array == NULL) {
		fprintf(stderr, "Unable to allocate memory\n");
		return 2;
	}

	for (int i = 0; i < array_size; ++i) {
		array[i] = 0;
	}

	int thread_count, alive_count;
	printf("enter thread count: ");
	scanf_s("%d", &thread_count);
	if (thread_count <= 0) {
		fprintf(stderr, "Incorrect thread count\n");
		return 3;
	}
	alive_count = thread_count;

	HANDLE* thread_handles = malloc(sizeof(HANDLE) * thread_count);
	DWORD* thread_ids = malloc(sizeof(DWORD) * thread_count);
	BOOL* alive_threads = malloc(sizeof(DWORD) * thread_count);
	int* thread_params = malloc(sizeof(int) * thread_count);
	if (thread_handles == NULL || thread_ids == NULL || thread_params == NULL) {
		fprintf(stderr, "Unable to allocate memory\n");
		return 4;
	}

	if (!create_events(thread_count))
		return 5;

	print_mutex = CreateMutex(NULL, FALSE, NULL);

	for (int i = 0; i < thread_count; ++i) {
		thread_params[i] = i + 1;
		thread_handles[i] = CreateThread(NULL, 0, do_work, &thread_params[i], 0, &thread_ids[i]);
		alive_threads[i] = TRUE;
	}
	for (int i = 0; i < thread_count; i++) {
		SetEvent(events_start[i]);
	}

	while (alive_count > 0) {
		for (int i = 0; i < array_size; i++) {
			if (alive_threads[i]) {
				WaitForSingleObject(events_ready[i], INFINITE);
			}
		}
		WaitForMultipleObjects(thread_count, events_blocked, TRUE, INFINITE);
		DWORD terminate_id = ask_terminate(alive_threads);
		SetEvent(events_terminate[terminate_id]);
		--alive_count;
		alive_threads[terminate_id] = FALSE;
		WaitForSingleObject(thread_handles[terminate_id], INFINITE);
		for (int i = 0; i < array_size; ++i) {
			printf("%d%c", array[i], i == array_size - 1 ? '\n' : ' ');
		}
		for (int i = 0; i < thread_count; ++i) {
			if (alive_threads[i]) {
				SetEvent(events_start[i]);
			}
		}
	}

	safe_print("bye-bye\n");
	free(array);
	for (int i = 0; i < thread_count; ++i) {
		CloseHandle(thread_handles[i]);
	}
	free(thread_handles);
	free(thread_ids);
	delete_events(thread_count);

	return 0;
}
