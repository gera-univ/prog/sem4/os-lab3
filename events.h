#pragma once

#include <windows.h>

HANDLE *events_start;
HANDLE *events_blocked;
HANDLE *events_terminate;
HANDLE *events_ready;

BOOL create_events(int thread_count);
void delete_events(int thread_count);